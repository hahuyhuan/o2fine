$(document).ready(function () {
    "use strict";



    var toggleAffix = function(affixElement, scrollElement, wrapper) {
  
        var height = affixElement.outerHeight(),
            top = wrapper.offset().top;
        
        if (scrollElement.scrollTop() >= top-80){
            wrapper.height(height);
            affixElement.addClass("affix");
        }
        else {
            affixElement.removeClass("affix");
            wrapper.height('auto');
        }
          
      };
      
    
      $('[data-toggle="affix"]').each(function() {
        var ele = $(this),
            wrapper = $('<div></div>');
        
        ele.before(wrapper);
        $(window).on('scroll resize', function() {
            toggleAffix(ele, $(this), wrapper);
        });
        
        // init
        toggleAffix(ele, $(window), wrapper);
      });




    $('.main-menu-btn').click(function() {
        $('#sidebar').toggleClass("active-nav");
        $('#my-container').toggleClass("active-cont");
    });
    //////// Main Slider //////////////////////////////////////////
    $('#my-slider').sliderPro({
        width: 1976,
        height:800,
        arrows: false,
        fade: true,
        autoHeight:true,
        centerImage:false,
        autoScaleLayers: false,
        buttons: true,
        thumbnailArrows: true,
        autoplay: true,
        slideSpeed : 300,
        breakpoints: {
            768: {
                height:800,
                arrows: false,
                fade: true,
                autoHeight:true,
                centerImage:false,
                autoScaleLayers: false,
                forceSize: 'fullWidth',
                buttons: false,
                thumbnailArrows: false,
                autoplay: true,
                slideSpeed : 300,
            }
        },
    });
	//////// Toast //////////////////////////////////////////
	var liveToastOBJ;
	if ($('#liveToast').length) {
	  var liveToast=$('#liveToast');
	  liveToastOBJ=new bootstrap.Toast(liveToast,{autohide:false});
      liveToastOBJ.show();
	}
    //toastr.info('Are you the 6 fingered man?');
    // -------------------------------------------------------------
	//   Doctor Slider
	// -------------------------------------------------------------
    $('.doctor-slider').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1,
                nav:false
            },
            992:{
                items:1
            }
        }
    })
    $('.news-slider').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        loop:false,
        responsive:{
            0:{
                items:1,
                nav:false
            },
            992:{
                items:1
            }
        }
    })
    // -------------------------------------------------------------
	//   Khoa Slider
	// -------------------------------------------------------------
    $('.khoa-slider').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots:true,
        nav:false,
        responsive:{
            0:{
                items:0
            },
            992:{
                items:4
            }
        }
    })
    $(window).scroll(function() {
        if($(window).scrollTop() != 0){
        $('#totop').fadeIn();
        }else {
        $('#totop').fadeOut();
        }
    });

    $('#totop').click(function() {
        $('html, body').animate({scrollTop:0},500);
    });
})
